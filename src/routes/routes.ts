import express from 'express';
import { error404, errorHandler } from '../controllers/error-handler';
import wrapper from '../middlewares/response-wrapper';
import { fetchHome } from '../controllers/home/home';
import { login } from '../controllers/auth/login';
import { signup } from '../controllers/auth/signup';
import { fetchUserProfile, updateUserProfile } from '../controllers/user/profile';
import { isAuthorizedRequest } from '../middlewares/token';
import { fetchProducts, fetchSingleProduct, searchProducts } from '../controllers/products/products';
import { addUserProduct, deleteUserProduct, fetchUserProducts, updateUserProduct } from '../controllers/user/user-products';
import { addToCart, deleteItemFromCart, fetchUserCart } from '../controllers/user/cart';
import { fetchUserOrders, fetchUserSales } from '../controllers/user/order';
import { fetchCategories, fetchCategoryProducts, fetchSubCategoryProducts } from '../controllers/categories/categories';
import { addReview, fetchReviews, updateReview } from '../controllers/review/review';
import { buyProductsFromCart } from '../controllers/products/buy';
import { recommendProducts } from '../controllers/products/recommend';

const router = express.Router();

// HomePage
router.get('/', wrapper(fetchHome));

// Auth
router.post('/login', wrapper(login));
router.post('/signup', wrapper(signup));

// Products
router.get('/products', isAuthorizedRequest, wrapper(fetchProducts));
router.get('/search/:query', isAuthorizedRequest, wrapper(searchProducts));
router.post('/products', isAuthorizedRequest, wrapper(addUserProduct));
router.patch('/products/:id', isAuthorizedRequest, wrapper(updateUserProduct));
router.get('/recommendations/:id', isAuthorizedRequest, wrapper(recommendProducts));
router.get('/product/:id', isAuthorizedRequest, wrapper(fetchSingleProduct));
router.put('/product/:id', isAuthorizedRequest, wrapper(updateUserProduct));
router.delete('/product/:id', isAuthorizedRequest, wrapper(deleteUserProduct));

// Categories
router.get('/categories', isAuthorizedRequest, wrapper(fetchCategories));
router.get('/category/:id', isAuthorizedRequest, wrapper(fetchCategoryProducts));
router.get('/subcategory/:id', isAuthorizedRequest, wrapper(fetchSubCategoryProducts));

// Reviews
router.get('/reviews/:id', isAuthorizedRequest, wrapper(fetchReviews));
router.post('/reviews/:id', isAuthorizedRequest, wrapper(addReview));
router.patch('/reviews/:id', isAuthorizedRequest, wrapper(updateReview));


// Buy
// router.post('/buy/:id', isAuthorizedRequest, wrapper(buyProduct));

// User
router.get('/user', isAuthorizedRequest, wrapper(fetchUserProfile));
router.patch('/user', isAuthorizedRequest, wrapper(updateUserProfile)); // make vendor
router.get('/user/products', isAuthorizedRequest, wrapper(fetchUserProducts));
router.get('/user/cart', isAuthorizedRequest, wrapper(fetchUserCart));
router.post('/user/cart/:id', isAuthorizedRequest, wrapper(addToCart));
router.delete('/user/cart/:cartId/:productId', isAuthorizedRequest, wrapper(deleteItemFromCart));
router.post('/user/order/:cart', isAuthorizedRequest, wrapper(buyProductsFromCart));
router.get('/user/orders', isAuthorizedRequest, wrapper(fetchUserOrders));
router.get('/user/sales/:id', isAuthorizedRequest, wrapper(fetchUserSales));

router.use(errorHandler);

router.use('/', error404);


export default router;