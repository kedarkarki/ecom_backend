import { Request, Response, NextFunction } from "express";
import Jwt from 'jsonwebtoken';
import { APIErrorCodes, ApiError, AuthError } from "../interface/api-error";
import env from "../services/env";

// Middleware for checking the api token for verifying requests from app
export const isAuthorizedRequest = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = req.headers.authorization as string | undefined;
        if (!token) {
            throw new AuthError('Please provide Authorization key in request header');
        }
        const clientToken = Jwt.verify(token.split(' ')[1], env.jwt_secret);
        if (clientToken) {
            req.body.payload = clientToken;
            return next();
        }
    } catch (e) {
        if (e instanceof ApiError) {
            return res.status(e.statusCode).json(e.response());
        }
        const errorCode = APIErrorCodes.auth_failure;
        return res.status(401).json(
            {
                error_code: errorCode.code,
                error_info: errorCode.info,
                error_message: 'Invalid api-key provided.'
            }
        );
    }

}