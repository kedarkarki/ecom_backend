import { NextFunction, Request, Response } from "express";
import Controller from "../interface/controller";

const wrapper = (controller: Controller) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await controller(req);
            return res.status(200).json(data);
        } catch (e) {
            return next(e);
        }
    }
}

export default wrapper;