/**
 * Postgres Database Client with Prisma ORM
 */

import { PrismaClient } from "@prisma/client";
import dotenv from 'dotenv';

// Configures the environment variables to be accessible for functions below
dotenv.config();

/**
 * Global client for development environment
 * to eliminate multiple instances of database
 */
declare global {
    var p: PrismaClient
}
let prisma: PrismaClient

if (process.env.NODE_ENV === 'production') {
    prisma = new PrismaClient()
}
// for dev 
else {
    if (!global.p) {
        global.p = new PrismaClient()
    }

    prisma = global.p
}

export default prisma

