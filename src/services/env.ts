/**
 * Environment variables
 */

import dotenv from 'dotenv';

dotenv.config();

export default {
    database_url: process.env.DATABASE_URL,
    node_env: process.env.NODE_ENV,
    jwt_secret: process.env.JWT_SECRET ?? '',
    port: process.env.PORT,
}