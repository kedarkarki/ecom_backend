import Controller from "../../interface/controller";
import prisma from "../../services/db-client";

export const fetchHome: Controller = async (req) => {
    const recommendations = await prisma.product.findMany({
        take: 10,
        include: {
            category: {
                include: {
                    subCategories: true,
                }
            }
        },
        orderBy: {
            updatedAt: 'desc'
        }
    });
    const bestSelling = await prisma.product.findMany({
        orderBy: {
            sales: {
                _count: 'desc',
            }
        },
        include: {
            category: {
                include: {
                    subCategories: true,
                }
            }
        },
        take: 10,
    });
    const topRated = await prisma.product.findMany({
        orderBy: {
            rating: 'desc',
        },
        include: {
            category: {
                include: {
                    subCategories: true,
                }
            }
        },
        take: 10,
    });
    return {
        msg: 'Home Fetched Successfully',
        data: {
            recommendations,
            topRated,
            bestSelling,
        }
    };
}