import { Product } from "@prisma/client";
import Controller from "../../interface/controller";
import PaginatedResponse from "../../interface/paginated-response";
import prisma from "../../services/db-client";
import { parseNumber } from "../../utils/number_util";
import { APIErrorCodes, ValidationError } from "../../interface/api-error";
import { ProductBody, validateProductBody } from "../../interface/product-body";

export const fetchUserProducts: Controller = async (req) => {
    const page = parseNumber(req.query.page, 1);
    const count = parseNumber(req.query.count, 10);
    const userId = req.body.payload.id as string;
    const products = await prisma.product.findMany(
        {
            where: {
                userId
            },
            skip: (page - 1) * count,
            take: count,
            include: {
                category: {
                    include: {
                        subCategories: true
                    }
                }
            }
        }
    );
    const hasNextPage = products.length === count;
    const next = hasNextPage ? `${req.path}?page=${page + 1}` : null;
    const response: PaginatedResponse = {
        page,
        count,
        hasNextPage,
        next,
        data: [...products]
    };
    return {
        msg: 'Products fetched successfully',
        data: {
            ...response
        }
    };
}

export const addUserProduct: Controller = async (req) => {
    const userId = req.body.payload.id as string;
    const body = validateProductBody(req.body);
    const product = await prisma.product.create({
        data: {
            ...body,
            rating: 0,
            category: {
                connect: {
                    id: body.category,
                }
            },
            user: {
                connect: {
                    id: userId
                }
            },
            brand: body.brand,
            subCategory: {
                connect: {
                    id: body.subCategory
                }
            }
        },
        select: {
            id: true,
            name: true
        }
    });
    return {
        msg: 'Products added successfully',
        data: {
            ...product
        }
    };
}
export const updateUserProduct: Controller = async (req) => {
    delete req.body.payload;
    const body = req.body as ProductBody;
    const product = await prisma.product.update({
        where: {
            id: req.params.id,
        },
        data: {
            ...body,
            category: !body.category ? undefined : {
                connect: {
                    id: body.category
                }
            },
            subCategory: !body.subCategory ? undefined : {
                connect: {
                    id: body.subCategory
                }
            },
        },
    });
    return {
        msg: 'Product updated successfully',
        data: {
            ...product
        }
    };
}

export const deleteUserProduct: Controller = async (req) => {
    const product = await prisma.product.delete({
        where: {
            id: req.params.id,
        },
    });
    return {
        msg: 'Product deleted successfully',
        data: {
            ...product
        }
    };
}

