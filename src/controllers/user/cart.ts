import { APIErrorCodes, DataEmptyError, ValidationError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import prisma from "../../services/db-client";
import { parseNumber } from "../../utils/number_util";

export const fetchUserCart: Controller = async (req) => {
    const userId = req.body.payload.id as string;
    const cart = await prisma.cart.findUnique({
        where: {
            userId
        },
        include: {
            user: {
                include: {
                    address: true
                }
            },
            items: {
                include: {
                    product: {
                        include: {
                            category: true,
                        }
                    },
                }
            }
        }
    });
    return {
        msg: 'User cart fetched successfully',
        data: {
            ...cart
        }
    };
}

export const addToCart: Controller = async (req) => {
    const productId = req.params.id;
    const productQty = parseNumber(req.body.quantity, 1);
    const userId = req.body.payload.id as string;
    if (!productId) {
        throw new ValidationError('Please provide product id', APIErrorCodes.incomplete_information);
    }
    const product = await prisma.product.findUnique({
        where: {
            id: productId
        },
        select: {
            id: true,
            price: true,
        }
    });
    if (!product) {
        throw new DataEmptyError('Requested product is not found', APIErrorCodes.not_found);
    }
    const cart = await prisma.cart.findUnique({
        where: {
            userId
        },
    });
    if (!cart) {
        throw new DataEmptyError('Requested cart is not found', APIErrorCodes.not_found);
    }
    const cartItem = await prisma.cartItem.findUnique({
        where: {
            cartId_productId: {
                cartId: cart.id,
                productId: product.id,
            }
        }
    });
    if (!cartItem) {
        // Product is new on cart
        await prisma.cartItem.create({
            data: {
                price: product.price,
                quantity: productQty,
                cartId: cart.id,
                productId: product.id,
            }
        });
    } else {
        // Product already exists on cart
        await prisma.cartItem.update({
            where: {
                cartId_productId: {
                    cartId: cart.id,
                    productId: product.id,
                }
            },
            data: {
                quantity: cartItem.quantity + productQty,
            }
        });
    }
    const updatedCart = await prisma.cart.findUnique({
        where: {
            userId
        },
        include: {
            items: true,
        }
    });

    return {
        msg: 'Product added to cart successfully',
        data: {
            ...updatedCart
        }
    };
}

export const deleteItemFromCart: Controller = async (req) => {
    const cartId = req.params.cartId;
    const productId = req.params.productId;
    const resp = await prisma.cartItem.delete({
        where: {
            cartId_productId: {
                cartId,
                productId
            }
        }
    });
    return {
        msg: 'Cart Item Deleted Successfully',
        data: {
            ...resp
        }
    };
}