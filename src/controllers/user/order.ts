import { APIErrorCodes, ApiError, DataEmptyError, ValidationError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import prisma from "../../services/db-client";

export const fetchUserOrders: Controller = async (req) => {
    const userId = req.body.payload.id as string;
    const orders = await prisma.order.findMany({
        where: {
            userId
        },
        include: {
            sales: {
                select: {
                    id: true
                }
            },

        }
    });
    if (!orders) {
        throw new DataEmptyError('No orders found for user.', APIErrorCodes.not_found);
    }
    return {
        msg: 'Orders fetched successfully',
        data: {
            orders
        }
    };
}

export const fetchUserSales: Controller = async (req) => {
    const userId = req.body.payload.id;
    const orderId = req.params.id;
    const sales = await prisma.sales.findMany({
        where: {
            orderId
        },
        include: {
            product: {
                include: {
                    category: true,
                    subCategory: true,
                    reviews: {
                        where: {
                            userId
                        },
                    },
                }
            }

        }
    });
    if (!sales) {
        throw new DataEmptyError('No products found for order.', APIErrorCodes.not_found);
    }
    return {
        msg: 'Ordered products fetched successfully',
        data: {
            sales
        }
    };
}
