import { add } from "winston";
import { APIErrorCodes, AuthError, ValidationError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import prisma from "../../services/db-client";
import { validateAddress } from "../../utils/address_validator";
import { mapStringToGender } from '../../utils/gender_util';

export const fetchUserProfile: Controller = async (req) => {
    const userId = req.body.payload.id as string;

    const user = await prisma.user.findUnique({
        where: {
            id: userId
        },
        select: {
            id: true,
            name: true,
            email: true,
            address: true,
            avatar: true,
            isVendor: true,
            isVerified: true,
            phone: true,
            gender: true,
            description: true,
        },

    });

    if (!user) {
        throw new AuthError('The user does not exist.');
    }
    return {
        msg: 'User profile fetched successfully.',
        data: {
            ...user
        }
    };
}

export const updateUserProfile: Controller = async (req) => {
    const userId = req.body.payload.id as string;

    const user = await prisma.user.findUnique({
        where: {
            id: userId
        },
        select: {
            id: true,
            name: true,
            email: true,
        },
    });

    if (!user) {
        throw new AuthError('The user does not exist.');
    }
    const { avatar, name, description, phone, gender, isVendor } = req.body;
    const address = validateAddress(req.body.address);
    const updatedUser = await prisma.user.update({
        where: {
            id: userId
        },
        data: {
            avatar,
            name,
            description,
            phone,
            gender: gender ? mapStringToGender(gender) : undefined,
            isVendor,
            address: address === undefined ? undefined : {
                upsert: {
                    create: {
                        ...address
                    },
                    update: {
                        ...address
                    }
                }
            }
        },
        select: {
            id: true,
            name: true,
            email: true,
            address: true,
            avatar: true,
            isVendor: true,
            isVerified: true,
            phone: true,
            gender: true,
            description: true,
        }
    });
    return {
        msg: 'User profile updated successfully.',
        data: {
            ...updatedUser
        }
    };
}
