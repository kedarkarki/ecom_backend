import { APIErrorCodes, DataEmptyError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import PaginatedResponse from "../../interface/paginated-response";
import prisma from "../../services/db-client";
import { parseNumber } from "../../utils/number_util";

export const fetchCategories: Controller = async (req) => {
    const categories = await prisma.category.findMany(
        {
            include: {
                subCategories: true,
            }
        }
    );
    return {
        msg: 'Categories fetched successfully',
        data: {
            categories: [...categories]
        }
    };
}

export const fetchCategoryProducts: Controller = async (req) => {
    const categoryId = req.params.id;
    const category = await prisma.category.findUnique({
        where: {
            id: categoryId
        },
        include: {
            products: true,
        }
    });
    if (!category) {
        throw new DataEmptyError('Requested category was not found',
            APIErrorCodes.not_found);
    }
    return {
        msg: 'Category products fetched successfully',
        data: {
            ...category
        }
    };
}

export const fetchSubCategoryProducts: Controller = async (req) => {
    const subId = req.params.id;
    const page = parseNumber(req.query.page, 1);
    const count = parseNumber(req.query.count, 10);
    const category = await prisma.subCategory.findUnique({
        where: {
            id: subId
        },
        include: {
            products: {
                include: {
                    category: true,
                },
                skip: (page - 1) * count,
                take: count,
            },
        }
    });

    if (!category) {
        throw new DataEmptyError('Requested category was not found',
            APIErrorCodes.not_found);
    }
    const hasNextPage = category.products.length === count;
    const next = hasNextPage ? `${req.path}?page=${page + 1}` : null;
    const response: PaginatedResponse = {
        page,
        count,
        hasNextPage,
        next,
        data: [...category.products]
    };
    return {
        msg: 'Category products fetched successfully',
        data: {
            ...response
        }
    };
}