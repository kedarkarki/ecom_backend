import { APIErrorCodes, DataEmptyError, ValidationError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import PaginatedResponse from "../../interface/paginated-response";
import prisma from "../../services/db-client";
import { parseNumber } from "../../utils/number_util";
import PythonInstance, { Python } from "../../utils/python";

export const fetchReviews: Controller = async (req) => {
    const productId = req.params.id;
    const page = parseNumber(req.query.page, 1);
    const count = parseNumber(req.query.count, 5);
    const reviews = await prisma.review.findMany({
        where: {
            productId
        },
        skip: (page - 1) * count,
        take: count,
    });
    const hasNextPage = reviews.length === count;
    const next = hasNextPage ? `${req.path}?page=${page + 1}` : null;
    const response: PaginatedResponse = {
        page,
        count,
        hasNextPage,
        next,
        data: [...reviews]
    };
    return {
        msg: 'Reviews fetched successfully',
        data: {
            ...response
        }
    };
}
export const fetchUserReview: Controller = async (req) => {
    const userId = req.body.payload.id;
    const productId = req.params.id;
    const review = await prisma.review.findUnique({
        where: {
            productId_userId: {
                productId,
                userId,
            }
        },
    });
    return {
        msg: 'Review fetched successfully',
        data: {
            ...review
        }
    };
}

export const addReview: Controller = async (req) => {
    const userId = req.body.payload.id;
    const productId = req.params.id;
    const review = validateReviewBody(req.body);
    const product = await prisma.product.findUnique({
        where: {
            id: productId
        },
        select: {
            rating: true,
            _count: {
                select: {
                    reviews: true,
                }
            }
        },

    });
    if (!product) {
        throw new DataEmptyError('Could not find requested product', APIErrorCodes.not_found);
    }
    const { data } = await PythonInstance.post(
        Python.SENTIMENT_PATH,
        {
            'text': review.content,
        },
        {
            headers: {
                'Content-Type': 'application/json'
            }
        }
    );
    const sentiment = data[0];
    const resp = await prisma.review.create({
        data: {
            ...review,
            userId,
            productId,
            sentiment: sentiment,
        }
    });

    const numerator = (product.rating * product._count.reviews) + review.rating;
    const denominator = product._count.reviews + 1;
    const newRating = numerator / denominator;
    await prisma.product.update({
        where: {
            id: productId
        },
        data: {
            rating: {
                set: newRating,
            }
        },
    });
    return {
        msg: 'Review added successfully',
        data: {
            ...resp
        }
    };
}

export const updateReview: Controller = async (req) => {
    const userId = req.body.payload.id;
    const productId = req.params.id;
    const review = validateReviewBody(req.body);
    const { data } = await PythonInstance.post(
        Python.SENTIMENT_PATH,
        {
            'text': review.content,
        },
        {
            headers: {
                'Content-Type': 'application/json'
            }
        }
    );
    const sentiment = data[0];
    const resp = await prisma.review.update({
        where: {
            productId_userId: {
                productId,
                userId,
            }
        },
        data: {
            ...review,
            sentiment: sentiment,
        }
    });
    return {
        msg: 'Review updated successfully',
        data: {
            ...resp
        }
    };
}

const validateReviewBody = (body: any) => {
    const { content, rating } = body;
    if (!content || !rating) {
        throw new ValidationError('Please provide content and rating', APIErrorCodes.bad_request);
    }
    return {
        content, rating,
    }
}