import Controller from "../../interface/controller";
import PaginatedResponse from "../../interface/paginated-response";
import prisma from "../../services/db-client";
import { parseNumber } from "../../utils/number_util";

export const fetchAnalyzedReviews: Controller = async (req) => {
    const productId = req.params.id;
    const page = parseNumber(req.query.page, 1);
    const count = parseNumber(req.query.count, 5);
    const reviews = await prisma.review.findMany({
        where: {
            productId,
        },
        skip: (page - 1) * count,
        take: count,
    });
    const analyzed = reviews.map(e => {
        return {
            review: { ...e },
            sentiment: 'positive',
        };
    });
    const hasNextPage = reviews.length === count;
    const next = hasNextPage ? `${req.path}?page=${page + 1}` : null;
    const response: PaginatedResponse = {
        page,
        count,
        hasNextPage,
        next,
        data: [...reviews]
    };
    return {
        msg: 'Reviews fetched successfully',
        data: {
            ...response
        }
    };
}