import { compare } from "bcrypt";
import { AuthError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import prisma from "../../services/db-client";
import Jwt from 'jsonwebtoken';
import env from "../../services/env";

export const login: Controller = async (req) => {
    const { email, password } = validateLoginParams(req.body);

    const user = await prisma.user.findUnique(
        {
            where: {
                email
            }
        }
    );

    if (!user) {
        throw new AuthError(`User with email ${email} does not exist. Please sign up first.`);
    }

    const isPasswordValid = await compare(password, user.password);

    if (!isPasswordValid) {
        throw new AuthError('Incorrect Password provided.');
    }

    const authToken = Jwt.sign({
        id: user.id,
        email,
        isVendor: user.isVendor,
    },
        env.jwt_secret
    );


    return {
        msg: 'Login Successful',
        data: {
            user: {
                id: user.id,
                name: user.name,
                gender: user.gender,
                email: user.email,
                avatar: user.avatar,
                description: user.description,
                phone: user.phone,
                isVerified: user.isVerified,
                isVendor: user.isVendor,
            },
            token: authToken,
        }
    };
}

const validateLoginParams = (body: any) => {
    const email = body.email as string | undefined;
    const password = body.password as string | undefined;

    if (!email || !password) {
        throw new AuthError('Please provide email or password');
    }

    return { email, password };

}