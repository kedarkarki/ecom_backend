import { hash } from "bcrypt";
import { AuthError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import prisma from "../../services/db-client";
import { mapStringToGender } from "../../utils/gender_util";
import { AddressRequest, validateAddress } from "../../utils/address_validator";

export const signup: Controller = async (req) => {
    const { email, password, avatar, gender, name, phone, address } = validateSignUpParams(req.body);

    const userExists = await prisma.user.findUnique(
        {
            where: {
                email
            }
        }
    );

    if (userExists) {
        throw new AuthError(`User with email ${email} already exists. Please login.`);
    }
    const encryptedPassword = await hash(password, 10);

    const user = await prisma.user.create({
        data: {
            email, avatar, name, phone,
            gender: mapStringToGender(gender),
            isVendor: false,
            isVerified: true,
            password: encryptedPassword,
            address: {
                create: address === undefined ? undefined : {
                    ...address,
                    id: undefined,
                }
            },
            cart: {
                create: {
                }
            }
        }
    });

    if (!user) {
        throw new AuthError(`Something went wrong. Please try again later.`);
    }

    return {
        msg: 'Signup Successful. Please login to continue.',
    };
}

const validateSignUpParams = (body: UserRequest): UserRequest => {
    const { email, password, avatar, gender, name, phone } = body;

    const address = validateAddress(body.address);


    if (!email || !password || !avatar || !gender || !name || !phone || !address) {
        throw new AuthError('Please provide email, password, avatar, gender, name, address and phone.');
    }

    return body;

}

interface UserRequest {
    name: string
    avatar: string
    email: string
    phone: string
    password: string
    gender: string
    address: AddressRequest | undefined
}