import { OrderStatus } from "@prisma/client";
import { APIErrorCodes, ServerError, ValidationError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import prisma from "../../services/db-client";
import { parseNumber } from "../../utils/number_util";

export const buyProductsFromCart: Controller = async (req) => {
    const userId = req.body.payload.id;
    const cartId = req.params.id;
    const amount = parseNumber(req.body.amount, 0);
    const status = req.body.status as OrderStatus | undefined;
    const cartItems = await prisma.cartItem.findMany({
        where: {
            cartId
        }
    });
    const buyRes = await prisma.order.create({
        data: {
            status: status ?? 'CONFIRMED',
            userId,
            amount,
            sales: {
                createMany: {
                    data: cartItems.map(e => {
                        return {
                            productId: e.productId,
                            quantity: e.quantity,
                            userId,
                        }
                    }),
                    skipDuplicates: true,
                }
            },
        }
    });
    if (!buyRes) {
        throw new ServerError('Could not create order');
    }
    if (status !== OrderStatus.PENDING) {
        await prisma.cartItem.deleteMany({
            where: {
                cartId: cartId,
            }
        });
    } else {
        await prisma.order.deleteMany({
            where: {
                status: OrderStatus.PENDING,
                userId
            }
        });
    }

    return {
        msg: 'Sales successfully recorded',
        data: {
            ...buyRes
        }
    };
}