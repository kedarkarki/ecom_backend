import { APIErrorCodes, DataEmptyError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import PaginatedResponse from "../../interface/paginated-response";
import prisma from "../../services/db-client";
import { parseNumber } from "../../utils/number_util";

export const fetchProducts: Controller = async (req) => {
    const page = parseNumber(req.query.page, 1);
    const count = parseNumber(req.query.count, 10);
    const products = await prisma.product.findMany(
        {
            skip: (page - 1) * count,
            take: count,
        }
    );
    const hasNextPage = products.length === count;
    const next = hasNextPage ? `${req.path}?page=${page + 1}` : null;
    const response: PaginatedResponse = {
        page,
        count,
        hasNextPage,
        next,
        data: [...products]
    };
    return {
        msg: 'Products fetched successfully',
        data: {
            ...response
        }
    };
}
export const fetchSingleProduct: Controller = async (req) => {
    const id = req.params.id;
    const product = await prisma.product.findUnique(
        {
            where: {
                id
            },
            include: {
                category: {
                    include: {
                        subCategories: true,
                    }
                },
                reviews: {
                    include: {
                        user: true,
                    }
                },
                user: true,
            }
        }
    );
    if (!product) {
        throw new DataEmptyError('Requested product was not found',
            APIErrorCodes.not_found);
    }
    return {
        msg: 'Product fetched successfully',
        data: {
            ...product
        }
    };
}


export const searchProducts: Controller = async (req) => {
    const page = parseNumber(req.query.page, 1);
    const count = parseNumber(req.query.count, 10);
    const query = req.params.query;
    const products = await prisma.product.findMany(
        {
            where: {
                name: {
                    contains: query,
                    mode: 'insensitive',
                },
            },
            skip: (page - 1) * count,
            take: count,
            include: {
                category: {
                    include: {
                        subCategories: true
                    }
                }
            }
        }
    );
    const hasNextPage = products.length === count;
    const next = hasNextPage ? `${req.path}?page=${page + 1}` : null;
    const response: PaginatedResponse = {
        page,
        count,
        hasNextPage,
        next,
        data: [...products]
    };
    return {
        msg: 'Products fetched successfully',
        data: {
            ...response
        }
    };
}