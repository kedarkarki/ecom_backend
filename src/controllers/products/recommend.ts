import { APIErrorCodes, DataEmptyError } from "../../interface/api-error";
import Controller from "../../interface/controller";
import prisma from "../../services/db-client";
import PythonInstance, { Python } from "../../utils/python";

export const recommendProducts: Controller = async (req) => {
    const id = req.params.id;
    const product = await prisma.product.findUnique(
        {
            where: {
                id
            },
            select: {
                name: true,
                category: true,
                subCategory: true,
            }
        }
    );
    if (!product) {
        throw new DataEmptyError('Requested product was not found',
            APIErrorCodes.not_found);
    }
    const { data } = await PythonInstance.post(
        Python.RECOMMEND_PATH,
        {
            'name': product.name,
        },
        {
            headers: {
                'Content-Type': 'application/json'
            }
        }
    );
    if (data.length === 0) {
        const dData = await prisma.product.findMany({
            where: {
                name: {
                    contains: product.name.split(' ')[0],
                    mode: 'insensitive',
                }
            },
            include: {
                category: {
                    include: {
                        subCategories: true,
                    }
                },
                reviews: {
                    include: {
                        user: true,
                    }
                },
                user: true,
            },
            take: 10,
        });
        return {
            msg: 'Recommendations fetched successfully',
            data: {
                recommendations: [...dData]
            }
        };
    }
    const dProds = [];
    for (const d of data) {
        const dbP = await prisma.product.findFirst({
            where: {
                name: d
            },
            include: {
                category: {
                    include: {
                        subCategories: true,
                    }
                },
                reviews: {
                    include: {
                        user: true,
                    }
                },
                user: true,
            },
        });
        if (dbP) {
            dProds.push(dbP);
        }
        if (dProds.length >= 10) {
            break;
        }
    }

    return {
        msg: 'Recommendations fetched successfully',
        data: {
            recommendations: [...dProds]
        }
    };
}
