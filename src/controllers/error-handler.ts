import { Request, Response, NextFunction } from "express";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime/library";
import { APIErrorCodes, ApiError } from "../interface/api-error";

export const error404 = (req: Request, res: Response) => {
    return res.status(404).json({
        error_code: '104',
        error_info: 'not_found',
        error_message: 'No such endpoint exists.'
    });
}

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
    console.log(err);
    if (err instanceof ApiError) {
        return res.status(err.statusCode).json(err.response());
    }
    if (err instanceof PrismaClientKnownRequestError) {
        return res.status(500).json({
            error_code: err.code,
            error_info: err.name,
            error_message: err.message,
        })
    }
    if (err instanceof URIError) {
        return res.status(400).json({
            error_code: APIErrorCodes.parsing_failure.code,
            error_info: APIErrorCodes.parsing_failure.info,
            error_message: err.message,
        });
    }
    return res.status(500).json({
        error_code: APIErrorCodes.server_failure.code,
        error_info: APIErrorCodes.server_failure.info,
        error_message: 'Interval Server Error. Please try again later',
    });
}