export class ApiError {
    message: string;
    statusCode: number;
    errorInfo: ApiErrorInfo;
    constructor(message: string, statusCode: number, info: ApiErrorInfo) {
        this.message = message;
        this.statusCode = statusCode;
        this.errorInfo = info;
    }

    response() {
        return {
            error_code: this.errorInfo.code,
            error_info: this.errorInfo.info,
            error_message: this.message
        }
    }
}

export class ValidationError extends ApiError {
    constructor(message: string, info: ApiErrorInfo) {
        super(message, 400, info);
    }
}
export class DataEmptyError extends ApiError {
    constructor(message: string, info: ApiErrorInfo) {
        super(message, 404, info);
    }
}

export class AuthError extends ApiError {
    constructor(message: string) {
        super(message, 401, APIErrorCodes.auth_failure);
    }
}
export class AccessError extends ApiError {
    constructor(message: string) {
        super(message, 403, APIErrorCodes.access_denied);
    }
}
export class ServerError extends ApiError {
    constructor(message: string) {
        super(message, 500, APIErrorCodes.server_failure);
    }
}

interface ApiErrorInfo {
    code: string;
    info: string;
}

export const APIErrorCodes = {
    auth_failure: {
        code: '101',
        info: 'authorization_failure'
    },
    incomplete_information: {
        code: '102',
        info: 'incomplete_information'
    },
    parsing_failure: {
        code: '103',
        info: 'parsing_failure'
    },
    not_found: {
        code: '104',
        info: 'not_found'
    },
    bad_request: {
        code: '105',
        info: 'invalid_information'
    },
    server_failure: {
        code: '106',
        info: 'server_failure'
    },
    access_denied: {
        code: '107',
        info: 'access_denied'
    }
};