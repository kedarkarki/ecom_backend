interface PaginatedResponse {
    page: number;
    count: number;
    hasNextPage: boolean;
    next?: string | null;
    data: any[]
}

export default PaginatedResponse;