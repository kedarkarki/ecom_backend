import { NextFunction, Request, Response } from "express";

interface ResponseFormat {
    msg: string;
    data?: any;
}

type Controller = (req: Request) => Promise<ResponseFormat>;

export default Controller;