import { APIErrorCodes, ValidationError } from "./api-error"

export interface ProductBody {
    image: string
    name: string
    description: string
    price: number
    originalPrice: number
    category: string
    brand: string
    stock: number
    subCategory: string
}

export const validateProductBody = (body: any) => {
    const { name, image, description, price, originalPrice, category, brand, subCategory, stock } = body;

    if (!name || !description || !price || !originalPrice || !category || !brand || !subCategory || !stock) {
        throw new ValidationError(
            'Product must have name,image, description, price, originalPrice, category id, brand id and subCategory id',
            APIErrorCodes.incomplete_information
        );
    }
    delete body.payload;
    return body;
}