import prisma from "../services/db-client";

const categories = [
    { "category": "Home Appliances", "sub": ["Heater", "Refrigerator", "Washing Machine", "Air Conditioner", "Vacuum Cleaner", "Kitchen", "Others", "Decorations"], "image": "https://www.svgrepo.com/show/494065/electric-socket.svg" },
    { "category": "Electronics", "sub": ["Smartphones", "Laptop", "Camera", "TV", "Smart Watch", "EarBuds", "HeadPhones", "Accessories", "Others"], "image": "https://www.svgrepo.com/show/447989/electronics.svg" },
    { "category": "Mens Clothing", "sub": ["Shirts", "Pants", "Footwear", "T-Shirt", "Jackets", "Sweaters", "Others"], "image": "https://www.svgrepo.com/show/156319/men-clothing.svg" },
    { "category": "Womens Clothing", "sub": ["Shirts", "Pants", "Footwear", "T-Shirt", "Jackets", "Sweaters", "Shorts", "Accessories", "Others"], "image": "https://www.svgrepo.com/show/399579/i-womens-health.svg" },
    { "category": "Sports", "sub": ["Boots", "Jerseys", "Shorts", "Tracks", "Accessories", "Proteins", "Watches", "Toys", "Others"], "image": "https://www.svgrepo.com/show/446025/sports-soccer.svg" },
    { "category": "Jewellery", "sub": ["Earrings", "Necklace", "Bracelets", "Rings", "Others"], "image": "https://www.svgrepo.com/show/513916/jewel-ring-diamond-2.svg" },
    { "category": "Others", "sub": ["Cleaning", "Others", "Baby Items", "Unisex Clothing", "Books", "Travels", "Vehicles"], "image": "https://www.svgrepo.com/show/513916/jewel-ring-diamond-2.svg" },
    { "category": "Health & Beauty", "sub": ["Perfumes", "Makeup", "Haircare", "Skincare", "Others", "Medical"], "image": "https://www.svgrepo.com/show/482559/makeup-brush-1.svg" },
    { "category": "Groceries", "sub": ["Foods", "Others"], "image": "https://www.svgrepo.com/show/482559/makeup-brush-1.svg" }

];

export const upCategory = async () => {
    for (const cat of categories) {
        await prisma.category.create({
            data: {
                name: cat.category,
                image: cat.image,
                subCategories: {
                    createMany: {
                        data: cat.sub.map(e => {
                            return {
                                name: e,
                                image: cat.image,
                            }
                        }),
                        skipDuplicates: true,
                    }
                }
            }
        });
    }
    console.log('DONE');

}

const sub = [
    { "name": "Heater", "image": "https://www.reshot.com/preview-assets/icons/FLC5EG4VU3/heater-FLC5EG4VU3.svg" },
    { "name": "Refrigerator", "image": "https://www.reshot.com/preview-assets/icons/JQMK9EY6SA/refrigerator-JQMK9EY6SA.svg" },
    { "name": "Washing Machine", "image": "https://www.reshot.com/preview-assets/icons/GCVKUPWTBN/washing-machine-GCVKUPWTBN.svg" },
    { "name": "Air Conditioner", "image": "https://www.reshot.com/preview-assets/icons/4JGSM6ZK35/air-conditioner-4JGSM6ZK35.svg" },
    { "name": "Vacuum Cleaner", "image": "https://www.reshot.com/preview-assets/icons/XHAT7PNQDM/vacuum-cleaner-XHAT7PNQDM.svg" },
    { "name": "Smartphones", "image": "https://www.reshot.com/preview-assets/icons/SKB7MZ8J2E/holding-smartphone-SKB7MZ8J2E.svg" },
    { "name": "Laptop", "image": "https://www.reshot.com/preview-assets/icons/DJN8AEZRUM/help-on-laptop-DJN8AEZRUM.svg" },
    { "name": "Camera", "image": "https://www.reshot.com/preview-assets/icons/FH3WT72GVP/camera-clicks-FH3WT72GVP.svg" },
    { "name": "TV", "image": "https://www.reshot.com/preview-assets/icons/RVZN8J9WSA/tv-RVZN8J9WSA.svg" },
    { "name": "Smart Watch", "image": "https://www.reshot.com/preview-assets/icons/L3N2FW94BS/smart-watch-L3N2FW94BS.svg" },
    { "name": "EarBuds", "image": "https://www.reshot.com/preview-assets/icons/NTL423CJ75/earbuds-NTL423CJ75.svg" },
    { "name": "HeadPhones", "image": "https://www.reshot.com/preview-assets/icons/JLA8FR6K5B/headphones-JLA8FR6K5B.svg" },
    { "name": "Shirts", "image": "https://www.reshot.com/preview-assets/icons/VWPEL3J7ZQ/shirt-VWPEL3J7ZQ.svg" },
    { "name": "Pants", "image": "https://www.reshot.com/preview-assets/icons/GYV9SF6R4P/pants-GYV9SF6R4P.svg" },
    { "name": "Footwear", "image": "https://www.reshot.com/preview-assets/icons/BLWZXKUPJ6/shoes-BLWZXKUPJ6.svg" },
    { "name": "T-Shirt", "image": "https://www.reshot.com/preview-assets/icons/V8GHN2DBYK/t-shirt-V8GHN2DBYK.svg" },
    { "name": "Jackets", "image": "https://www.reshot.com/preview-assets/icons/5QXW2LPKTJ/warm-jacket-5QXW2LPKTJ.svg" },
    { "name": "Sweaters", "image": "https://www.reshot.com/preview-assets/icons/MF493HXQZP/sweater-MF493HXQZP.svg" },
    { "name": "Shorts", "image": "https://www.reshot.com/preview-assets/icons/J9AMTPQRKH/shorts-J9AMTPQRKH.svg" },
    { "name": "Boots", "image": "https://www.reshot.com/preview-assets/icons/J5HK8B6CGQ/football-shoes-J5HK8B6CGQ.svg" },
    { "name": "Jerseys", "image": "https://www.reshot.com/preview-assets/icons/MZCWJUNFV2/jersey-basketball-MZCWJUNFV2.svg" },
    { "name": "Tracks", "image": "https://www.reshot.com/preview-assets/icons/LB982HRTGJ/track-suit-LB982HRTGJ.svg" },
    { "name": "Accessories", "image": "https://www.reshot.com/preview-assets/icons/TYLEV8BGRZ/belt-TYLEV8BGRZ.svg" },
    { "name": "Proteins", "image": "https://www.reshot.com/preview-assets/icons/GTVDJWM2XF/whey-protein-GTVDJWM2XF.svg" },
    { "name": "Earrings", "image": "https://www.reshot.com/preview-assets/icons/V5AJWLE39R/earrings-V5AJWLE39R.svg" },
    { "name": "Necklace", "image": "https://www.reshot.com/preview-assets/icons/YHUEXZWRQ2/necklace-YHUEXZWRQ2.svg" },
    { "name": "Bracelets", "image": "https://www.reshot.com/preview-assets/icons/ZQ42WCEGD9/bracelet-ZQ42WCEGD9.svg" },
    { "name": "Rings", "image": "https://www.reshot.com/preview-assets/icons/R6ASDBC3ZM/ring-wedding-R6ASDBC3ZM.svg" },
    { "name": "Perfumes", "image": "https://www.reshot.com/preview-assets/icons/JDHMLN5RZV/perfume-JDHMLN5RZV.svg" },
    { "name": "Makeup", "image": "https://www.reshot.com/preview-assets/icons/TL9JHQ3Y6A/makeup-TL9JHQ3Y6A.svg" },
    { "name": "Haircare", "image": "https://www.reshot.com/preview-assets/icons/G75C6P8SNR/hair-drying-G75C6P8SNR.svg" },
    { "name": "Skincare", "image": "https://www.reshot.com/preview-assets/icons/HPBC4FEU8J/helping-hand-HPBC4FEU8J.svg" }
];

export const upSub = async () => {
    for (const s of sub) {
        const ss = await prisma.subCategory.findFirst({
            where: {
                name: s.name,
            }
        });
        if (!ss) {
            continue;
        }
        await prisma.subCategory.update({
            where: {
                id: ss.id,
            },
            data: {
                image: s.image
            }
        });
    }
}