import { ProductBody, validateProductBody } from "../interface/product-body";
import prisma from "../services/db-client";
import electronics from './electronics.json';

const getRandomNumber = (min: number, max: number) => {
    return Math.random() * (max - min) + min;
}

export const addDummyProduct = async () => {
    for (const p of electronics) {
        const res = await addUserProduct({
            name: p.NAME,
            brand: p["BRAND NAME"],
            category: p.CATEGORY,
            subCategory: p["SUB-CATEGORY"],
            description: p.DESCRIPTION,
            price: p.PRICE,
            originalPrice: p.PRICE * getRandomNumber(1.1, 1.7),
            stock: p.Stock,
            image: p["IMAGE URL"],
        });
        console.log(res);
    }

}
const addUserProduct = async (productBody: ProductBody) => {
    const userId = '362ec947-e2e4-4961-82f3-5cfb52e00fcc';
    const body = validateProductBody(productBody);
    const subCat = await prisma.subCategory.findFirst({
        where: {
            name: body.subCategory
        }
    });
    if (!subCat) {
        return { msg: 'No sub category found' };
    }
    const product = await prisma.product.create({
        data: {
            ...body,
            rating: 0,
            category: {
                connect: {
                    name: body.category,
                }
            },
            user: {
                connect: {
                    id: userId
                }
            },
            brand: body.brand,
            subCategory: {
                connect: {
                    id: subCat.id,
                }
            }
        },
        select: {
            id: true,
            name: true
        }
    });
    return {
        msg: 'Products added successfully',
        data: {
            ...product
        }
    };
}