import { writeFileSync } from 'fs';
import prisma from '../../services/db-client';
import products from './manjesh.json';

const getRandomNumber = (min: number, max: number) => {
    return Math.random() * (max - min) + min;
}
export const importManjeshProducts = async () => {
    let count = 0;
    for (const prod of products) {
        const newCat = prod.CATEGORY;
        const dCat = await prisma.category.findFirst({
            where: {
                name: newCat,
            }
        });
        if (!dCat) {
            console.log(`Not found category ${newCat}`);
            continue;
        }
        const subCat = await prisma.subCategory.findFirst({
            where: {
                name: prod['SUB-CATEGORY'],
                category: {
                    name: newCat,
                }
            }
        });
        if (!subCat) {
            console.log(`Not found subcategory ${prod['SUB-CATEGORY']}`);
            continue;
        }
        if (count % 10 == 0) {
            console.log(count);
        }
        ++count;
        await prisma.product.create({
            data: {
                name: prod.NAME,
                brand: prod.BRANDNAME ?? 'No Brand',
                description: prod.DESCRIPTION,
                image: prod.IMAGEURL ?? prod['IMAGE URL'] ?? '',
                originalPrice: prod.PRICE * getRandomNumber(1.1, 1.7),
                price: prod.PRICE,
                rating: 0,
                stock: prod.Stock,

                category: {
                    connect: {
                        id: dCat.id,
                    }
                },
                user: {
                    connect: {
                        id: 'f3fcc3f8-cadc-4272-99c9-ebce35d1c4fb',
                    }
                },
                subCategory: {
                    connect: {
                        id: subCat.id,
                    }
                }

            }
        });
    }
    console.log('DONE');
}

export const forBikasheyMuji = async () => {
    console.log('START');
    const prods: any[] = await prisma.product.findMany({
        include: {
            category: {
                select: {
                    name: true
                }
            },
            subCategory: {
                select: {
                    name: true
                }
            },
        }
    });
    for (const prod of prods) {
        prod.category = prod.category.name;
        prod.subCategory = prod.subCategory.name;
    }
    console.log(prods);
    // writeFileSync('./bikashProducts.json', JSON.stringify(prods));
}