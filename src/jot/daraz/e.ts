import prisma from '../../services/db-client';
import categories from './categories.json';
import products from './final.json';

export const importCategories = async () => {
    const cats: any = categories;
    for (const key of Object.keys(cats)) {
        const subCategories = cats[key];
        await prisma.category.create({
            data: {
                name: key,
                image: '',
                subCategories: {
                    createMany: {
                        data: subCategories.map((e: any) => {
                            return {
                                name: e,
                                image: '',
                            }
                        }),
                        skipDuplicates: true,
                    }
                }
            }
        });
        console.log('CREATED ' + key);
    }
    console.log('DONE');
}

export const importProducts = async () => {
    const prods: any = products;
    for (const prod of prods) {
        let newCat = prod.category;
        if (newCat === 'mens Clothing') {
            newCat = 'Mens Clothing';
        }
        const dCat = await prisma.category.findFirst({
            where: {
                name: newCat,
            }
        });
        if (!dCat) {
            console.log(`Not found category ${newCat}`);
            continue;
        }
        const subCat = await prisma.subCategory.findFirst({
            where: {
                name: prod.subCategory,
                category: {
                    name: newCat,
                }
            }
        });
        if (!subCat) {
            console.log(`Not found subcategory ${prod.subCategory}`);
            continue;
        }
        await prisma.product.create({
            data: {
                ...prod,
                category: {
                    connect: {
                        id: dCat.id,
                    }
                },
                user: {
                    connect: {
                        id: '1c6a2623-1441-467e-8d07-aa0dc4459f30',
                    }
                },
                subCategory: {
                    connect: {
                        id: subCat.id,
                    }
                }

            }
        });
    }
    console.log('DONE');
}

export const importProductsRemote = async () => {
    const prods: any = products;
    const listProds = [];
    let count = 0;
    for (const prod of prods) {
        let newCat = prod.category;
        if (newCat === 'mens Clothing') {
            newCat = 'Mens Clothing';
        }
        const dCat = await prisma.category.findFirst({
            where: {
                name: newCat,
            }
        });
        if (!dCat) {
            console.log(`Not found category ${newCat}`);
            continue;
        }
        const subCat = await prisma.subCategory.findFirst({
            where: {
                name: prod.subCategory,
                category: {
                    name: newCat,
                }
            }
        });
        if (!subCat) {
            console.log(`Not found subcategory ${prod.subCategory}`);
            continue;
        }
        const fProd = {
            ...prod,
            category: {
                connect: {
                    id: dCat.id,
                }
            },
            user: {
                connect: {
                    id: '1c6a2623-1441-467e-8d07-aa0dc4459f30',
                }
            },
            subCategory: {
                connect: {
                    id: subCat.id,
                }
            }
        };
        listProds.push(fProd);
        console.log(++count);
    }
    console.log('Start Bulk');
    const res = await prisma.product.createMany({
        data: [...listProds],
        skipDuplicates: true,
    });
    console.log(res);
    console.log('DONE');
}
