import { writeFileSync } from 'fs';
import categories from './categories.json';

export const cSub = () => {
    const forCat: any = {};
    const cats = categories as any;
    for (const c of Object.keys(cats)) {
        forCat[c] = [];
        for (const scat of cats[c]) {
            forCat[c].push({
                name: scat,
                image: '',
            })
        }
    }
    writeFileSync('./imagecategory.json', JSON.stringify(forCat));
}