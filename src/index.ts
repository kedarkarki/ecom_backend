import express from 'express';
import cors from 'cors';
// import winston from 'winston';
// import expressWinston from 'express-winston';
import env from './services/env';
import router from './routes/routes';

const app = express();

app.use(cors());

// For parsing the json body in POST request
app.use(express.json());

// For serving static files for swagger docs
app.use(express.static("public"));

// Starts the cron job for updating database periodically
// startCron();

// Swagger docs serving
// app.use(
//     "/docs",
//     swaggerUi.serve,
//     swaggerUi.setup(undefined, {
//         swaggerOptions: {
//             url: "/swagger.json",
//         },
//     })
// );

// winston logger for logging in production
// app.use(expressWinston.logger({
//     transports: [
//         new winston.transports.Console()
//     ],
//     format: winston.format.json(),
//     meta: false, // optional: control whether you want to log the meta data about the request (default to true)
//     msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
//     expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
//     colorize: false,
// }));

// App's main router for all types of endpoints
// app.use(cache('5 minutes'));
app.use(router);

const PORT = env.port || 3000;


app.listen(PORT, () => {
    console.log('App Listening on ' + PORT);
});