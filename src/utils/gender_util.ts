import { Gender } from "@prisma/client";

export const mapStringToGender = (gender: string): Gender => {
    switch (gender.toLowerCase()) {
        case 'female':
            return Gender.FEMALE;
        case 'other':
            return Gender.OTHER;
        default:
            return Gender.MALE;
    }
}