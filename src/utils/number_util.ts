
/**
 * 
 * @param value string formatted with commas
 * @returns parsed number removing all formatting
 */
export const parseNumber = (value: any, orElse: number): number => {
    if (!value) {
        return orElse;
    }
    let num = +value;
    if (isNaN(num)) {
        return orElse;
    }
    return num;
}