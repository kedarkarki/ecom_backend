import axios from "axios";

/**
 * Nrb endpoint for forex data
 */
export const Python = {
    BASE_URL: 'http://204.12.229.208:5001/',
    SENTIMENT_PATH: 'sent',
    RECOMMEND_PATH: 'recommend',
}
const PythonInstance = axios.create({
    baseURL: Python.BASE_URL,
});

export default PythonInstance;