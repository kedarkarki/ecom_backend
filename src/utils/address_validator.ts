import { APIErrorCodes, ValidationError } from "../interface/api-error";

export const validateAddress = (address: AddressRequest | undefined) => {
    if (!address) {
        return undefined;
    }
    const { street, city, district, province } = address;
    if (!street || !city || !district || !province) {
        throw new ValidationError('Address must include street, city, district and province',
            APIErrorCodes.incomplete_information
        );
    }
    return { ...address };
}

export interface AddressRequest {
    street: string
    city: string
    district: string
    province: string
}